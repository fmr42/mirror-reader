import os
import sys

import numpy as np
import matplotlib.pyplot as plt

from pathlib import Path
from collections import Counter

import tensorflow as tf
from tensorflow import keras
import cv2
# Path to the data directory
#data_dir = Path("/home/tera/Desktop/CAPTCH/renamed")
import captcha_lib



model=captcha_lib.build_model()
model.load_weights("captcha.h5")
# Get the prediction model by extracting layers till the output layer
prediction_model = keras.models.Model(
    model.get_layer(name="image").input, model.get_layer(name="dense2").output
)


mirror=False
cam = cv2.VideoCapture(0)
while True:
    ret_val, img = cam.read()
    if mirror:
        img = cv2.flip(img, 1)
    print(img.shape)
    #img = img[0:310, 0:100]
    img_pp = tf.image.convert_image_dtype(img, tf.float32)
    img_pp = tf.image.resize(img, [100, 310])
    img_pp = tf.transpose(img, perm=[1, 0, 2])
    cv2.imshow('my webcam', img)
    #preds = prediction_model.predict([img_pp])
    #pred_texts = captcha_lib.decode_batch_predictions(preds)
    if cv2.waitKey(1) == 27:
        break  # esc to quit
cv2.destroyAllWindows()
sys.exit()


