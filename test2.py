import random
import sys

import matplotlib.pyplot as plt
import string
import keras_ocr
import matplotlib
import cv2
from keras import Model

# Get a set of three example images

import os
#
# images = [
#     keras_ocr.tools.read(url) for url in [
# #        'https://upload.wikimedia.org/wikipedia/commons/b/bd/Army_Reserves_Recruitment_Banner_MOD_45156284.jpg',
# #        #'https://upload.wikimedia.org/wikipedia/commons/e/e8/FseeG2QeLXo.jpg',
# #        'https://upload.wikimedia.org/wikipedia/commons/b/b4/EUBanana-500x112.jpg'
#         '/home/tera/Desktop/k2/220516/CAM2/000017_CAM2_COLOR_OK.jpg',
#         '/home/tera/Desktop/k2/220516/CAM2/000017_CAM2_COLOR_OK.jpg'
#         #'/home/tera/Desktop/k2/220516/CAM2/000037_CAM2_COLOR_OK.jpg'
#     ]
# ]
import glob



recognizer = keras_ocr.recognition.Recognizer()
#alphabet = string.ascii_letters + string.digits + '+*=-'
alphabet = string.digits + string.ascii_letters
alphabet = ''.join(sorted(set(alphabet.lower())))
recognizer = keras_ocr.recognition.Recognizer(alphabet=alphabet,weights='kurapan')

recognizer.compile()
#img_list = glob.glob("/home/tera/Desktop/k2_color/sn/batch_220524a/*.jpg")
#for f in glob.iglob('/home/tera/Desktop/k2_color/dx/*/*',recursive=True):

img_list=[]
for f in glob.iglob('/home/tera/Desktop/k2_50g/train/dx/220329/*/*', recursive=True):
#for f in glob.iglob('/home/tera/Desktop/k2_50g/train/sn/*/*/*', recursive=True):
    if not os.path.isfile(f):
        continue
    if not f.endswith('jpg'):
        continue
    img_list.append(f)

recognizer.model.load_weights('recognizer_borndigital.h5')
pipeline = keras_ocr.pipeline.Pipeline(recognizer=recognizer)
img_list_4 = random.choices(img_list, k=4)
images = [ keras_ocr.tools.read(url) for url in img_list_4 ]

prediction_groups = pipeline.recognize(images)

plt.ion()
fig, ax = plt.subplots(nrows=1, figsize=(30, 30),num=1)
i=0
im_path = img_list[i]
im = [keras_ocr.tools.read(im_path)]
preds = pipeline.recognize(im)

while True:
    fig.clf()
    ax.clear()
    fig, ax = plt.subplots(nrows=1, figsize=(30, 30), num=1)
    keras_ocr.tools.drawAnnotations(image=im[0], predictions=preds[0], ax=ax)
    # plt.plot(1)
    plt.show(block=False)
    i=i+1
    im_path = img_list[i]
    im = [keras_ocr.tools.read(im_path)]
    preds = pipeline.recognize(im)
    if (plt.waitforbuttonpress(-1)):
        break
