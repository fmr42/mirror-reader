import sys
import zipfile
import datetime
import string
import math
import os

import tqdm
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn.model_selection

import keras_ocr

assert tf.test.is_gpu_available(), 'No GPU is available.'




# ===========================================================================
# ===========================================================================
#       PREPARE ALL DATA
# ===========================================================================
# ===========================================================================

data_dir = '.'
alphabet_raw = string.digits + string.ascii_uppercase
alphabet = ''.join(sorted(set(alphabet_raw)))
import pickle

from_scratch=False
import dill
fonts=None
if from_scratch:
    fonts = keras_ocr.data_generation.get_fonts(
        alphabet=alphabet,
        cache_dir=data_dir
    )
    dill.dump(fonts, file = open("fonts.dill", "wb"))
else:
    fonts = dill.load(open("fonts.dill", "rb"))

backgrounds = keras_ocr.data_generation.get_backgrounds(cache_dir='./cache_backgrounds')



import essential_generators

import random
import string
def argor_code_generator(min,max,population_alphabet):
    population = population_alphabet
    charlist = [random.choice(population) for i in range(min, max)]
    return "".join(charlist)

def get_text_generator_argor(gen_alphabet):
    while True:
        sentence = argor_code_generator(1,6,gen_alphabet)
        #sentence = "".join([s for s in sentence if (alphabet is None or s in alphabet)])
        #if max_string_length is not None:
        #    sentence = sentence[:max_string_length]
        yield sentence

text_generator = get_text_generator_argor(alphabet)

print("Current alphabet is:")
print(alphabet)
print('The first generated text is:', next(text_generator))

def get_train_val_test_split(arr):
    train, valtest = sklearn.model_selection.train_test_split(arr, train_size=0.8, random_state=42)
    val, test = sklearn.model_selection.train_test_split(valtest, train_size=0.5, random_state=42)
    return train, val, test

background_splits = get_train_val_test_split(backgrounds)
font_splits = get_train_val_test_split(fonts)

image_generators = [
    keras_ocr.data_generation.get_image_generator(
        height=640,
        width=640,
        text_generator=text_generator,
        font_groups={
            alphabet: current_fonts
        },
        backgrounds=current_backgrounds,
        font_size=(60, 120),
        margin=50,
        rotationX=(-0.05, 0.05),
        rotationY=(-0.05, 0.05),
        rotationZ=(-15, 15)
    )  for current_fonts, current_backgrounds in zip(
        font_splits,
        background_splits
    )
]

## See what the first validation image looks like.
#image, lines = next(image_generators[1])
#text = keras_ocr.data_generation.convert_lines_to_paragraph(lines)
#print('The first generated validation image (below) contains:', text)
#plt.imshow(image)

# ===========================================================================
# ===========================================================================
#       CREATE DETECTOR AND RECOGNIZER
# ===========================================================================
# ===========================================================================

detector = keras_ocr.detection.Detector(weights='clovaai_general')
recognizer = keras_ocr.recognition.Recognizer(
    alphabet=alphabet,
    weights='kurapan'
)
recognizer.compile()

#detector.model.load_weights('detector_last.h5')



# ===========================================================================
# ===========================================================================
#       FIT DETECTOR
# ===========================================================================
# ===========================================================================

for layer in recognizer.backbone.layers:
    layer.trainable = False
detector_batch_size = 1
detector_basepath = os.path.join(data_dir, f'detector_{datetime.datetime.now().isoformat()}')
detection_train_generator, detection_val_generator, detection_test_generator = [
    detector.get_batch_generator(
        image_generator=image_generator,
        batch_size=detector_batch_size
    ) for image_generator in image_generators
]
import multiprocessing
if True:
    detector.model.fit(
        detection_train_generator,
        steps_per_epoch=math.ceil(len(background_splits[0]) / detector_batch_size),
        epochs=1,
        #workers=multiprocessing.cpu_count(),
        workers=1,
        callbacks=[
            tf.keras.callbacks.EarlyStopping(restore_best_weights=False, patience=10),
#            tf.keras.callbacks.CSVLogger(f'{detector_basepath}.csv'),
#            tf.keras.callbacks.ModelCheckpoint(filepath=f'{detector_basepath}.h5')
        ],
        validation_data=detection_val_generator,
        validation_steps=math.ceil(len(background_splits[1]) / detector_batch_size),
        batch_size=detector_batch_size
    )
detector.model.save("detector_end_to_end.h5")


# ===========================================================================
# ===========================================================================
#       FIT RECOGNIZER
# ===========================================================================
# ===========================================================================
target_width=recognizer.model.input_shape[2]
target_height=recognizer.model.input_shape[1]
print("target width=")
print(target_width)
print("target height=")
print(target_height)

max_length = 10
recognition_image_generators = [
    keras_ocr.data_generation.convert_image_generator_to_recognizer_input(
        image_generator=image_generator,
        max_string_length=min(recognizer.training_model.input_shape[1][1], max_length),
        target_width=recognizer.model.input_shape[2],
        target_height=recognizer.model.input_shape[1],
        margin=1
    ) for image_generator in image_generators
]

# See what the first validation image for recognition training looks like.
image, text = next(recognition_image_generators[1])
print('This image contains:', text)
plt.imshow(image)
recognition_batch_size = 8
recognizer_basepath = os.path.join(data_dir, f'recognizer_{datetime.datetime.now().isoformat()}')
recognition_train_generator, recognition_val_generator, recognition_test_generator = [
    recognizer.get_batch_generator(
    image_generator=image_generator,
    batch_size=recognition_batch_size,
    ) for image_generator in recognition_image_generators
]
recognizer.training_model.fit(
    recognition_train_generator,
    epochs=100,
    steps_per_epoch=math.ceil(len(background_splits[0]) / recognition_batch_size),
    callbacks=[
        tf.keras.callbacks.EarlyStopping(restore_best_weights=False, patience=10),
#        tf.keras.callbacks.CSVLogger(f'{recognizer_basepath}.csv', append=True),
#        tf.keras.callbacks.ModelCheckpoint(filepath=f'{recognizer_basepath}.h5')
    ],
    validation_data=recognition_val_generator,
    validation_steps=math.ceil(len(background_splits[1]) / recognition_batch_size),
    workers=0
    #bacth_size=recognition_batch_size
)
recognizer.model.save('recognizer_end_to_end.h5')


