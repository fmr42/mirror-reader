import sys
import zipfile
import datetime
import string
import math
import os

import tqdm
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn.model_selection

import keras_ocr

#assert tf.test.is_gpu_available(), 'No GPU is available.'




# ===========================================================================
# ===========================================================================
#       PREPARE ALL DATA
# ===========================================================================
# ===========================================================================

data_dir = '.'
alphabet_raw = string.digits + string.ascii_lowercase #+ string.ascii_uppercase
#recognizer_alphabet = ''.join(sorted(set(alphabet.lower())))
alphabet = ''.join(sorted(set(alphabet_raw)))

# ===========================================================================
# ===========================================================================
#       CREATE DETECTOR AND RECOGNIZER
# ===========================================================================
# ===========================================================================

detector = keras_ocr.detection.Detector(weights='clovaai_general')
recognizer = keras_ocr.recognition.Recognizer(
    alphabet=alphabet,
    weights='kurapan'
)
recognizer.compile()

# ===========================================================================
# ===========================================================================
#       TEST THE PIPELINE
# ===========================================================================
# ===========================================================================

import glob
import random

detector.model.load_weights('detector.h5')
#recognizer.model.load_weights('recognizer.h5')
pipeline = keras_ocr.pipeline.Pipeline(detector=detector, recognizer=recognizer)
img_list = glob.glob("color_train/*.jpg")
img_list = random.choices(img_list,k=2)
images = [
    keras_ocr.tools.read(url) for url in img_list
]
prediction_groups = pipeline.recognize(images)
for image, predictions in zip(images, prediction_groups):
    fig, ax = plt.subplots(nrows=1, figsize=(30, 30))
    keras_ocr.tools.drawAnnotations(image=image, predictions=predictions, ax=ax)
    plt.plot
    plt.show(block=True)
    plt.close()







