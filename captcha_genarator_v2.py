import sys

import cv2
import numpy
import numpy as np
chars_path          ="/home/tera/Desktop/data/chars/"
backgrounds_path    ="/home/tera/Desktop/data/backgrounds/"
save_path           ="/home/tera/Desktop/data/generated/"
from random import seed, randint
from random import random
# generate random integer values
from random import seed
from random import randint
# seed random number generator
seed(1)
char_dict={}
char_dict_alpha={}
backgrounds={}
c_rand_pos=5
apply_noise=True


def randomize_brightness( img ):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)
    value=random()+0.2
    v_type=v.dtype
    v=(v*value)
    v[v > 200] = 200
    v[v < 0 ] = 0
    v = v.astype(v_type)
    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def noisy(noise_typ,image):
    i_type = image.dtype
    if noise_typ == "gauss":
        row,col,ch= image.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image + gauss
        return noisy.astype(i_type)
    elif noise_typ == "s&p":
        row,col,ch = image.shape
        s_vs_p = 0.8
        amount = 0.1
        s_vs_p = 0.5
        amount = 0.01
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt)) for i in image.shape]
        out[coords] = 1
        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper)) for i in image.shape]
        out[coords] = 0
        return out.astype(i_type)
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy.astype(i_type)
    elif noise_typ =="speckle":
        row,col,ch = image.shape
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = image + image * gauss
        return noisy.astype(i_type)


for c in ['A', 'C', 'E', 'F', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
    print(c)
    for v in [0,1]:
        char_dict[c][v]       = cv2.imread(chars_path+c+"_"+str(v).zfill(3)+".png", cv2.IMREAD_UNCHANGED)
        char_dict_alpha[c][v] = char_dict[c][:,:,3] / 255.0
for v in [0,1]:
    backgrounds[v] = cv2.imread(backgrounds_path+"bg_"+ str(v).zfill(3)+".png", cv2.IMREAD_COLOR)

rows,cols,channels = char_dict['0'].shape

#prefixes=['AE','AF','CC','CT']

range_max=9999
for p1 in ['A', 'C', 'E', 'F']:
    for p2 in ['A', 'C', 'E', 'F']:
    for s in range(range_max):
        progress=s/range_max
        print("Progress: " , np.round(progress*100)/100, "%", end='\r')
        off_y = 40 + randint(-10,10)
        off_x = 20 + randint(-10,10)
        im=backgrounds[ randint(0, len(backgrounds)-1) ].copy()
        min_zoom=1
        max_zoom=4
        rnd_zoom_x = min_zoom + (random() * (max_zoom - min_zoom))
        rnd_zoom_y = min_zoom + (random() * (max_zoom - min_zoom))
        im = cv2.resize(im, None, fx=rnd_zoom_x, fy=rnd_zoom_y)
        center = im.shape
        w = 310
        h = 100
        x = center[1] / 2 - w / 2
        y = center[0] / 2 - h / 2
        im = im[int(y):int(y + h), int(x):int(x + w)]
        prefix=[p1,p2]
        s_str=prefix+str(s).rjust(4,'0')
        for c in range(len(s_str)):
            rand_x = +randint(-c_rand_pos, c_rand_pos)
            rand_y = +randint(-c_rand_pos, c_rand_pos)
            center = im.shape
            tmp = char_dict[s_str[c]].copy()
            tmp_alpha=char_dict_alpha[s_str[c]].copy()
            for color in range(0, 3):
                im[off_x+rand_x:off_x+rand_x+rows, off_y+rand_y:off_y+rand_y+cols, color] = \
                    tmp_alpha * tmp[:, :, color] +  \
                    im[off_x+rand_x:off_x+rand_x+rows, off_y+rand_y:off_y+rand_y+cols, color] * (1 - tmp_alpha)
            off_y+=40
        min_zoom=1.0
        max_zoom=1.1
        rnd_zoom_x = min_zoom + (random() * (max_zoom - min_zoom))
        rnd_zoom_y = min_zoom + (random() * (max_zoom - min_zoom))
        im=cv2.resize(im, None, fx=rnd_zoom_x, fy=rnd_zoom_y)
        center = im.shape
        w=310
        h=100
        x = center[1]/2 - w / 2
        y = center[0]/2 - h / 2
        im = im[int(y):int(y + h), int(x):int(x + w)]
        if apply_noise:
            im = randomize_brightness(im)
            im = noisy("gauss", im)
            im = noisy("poisson", im)
            im = noisy("s&p", im)
            #im = noisy("speckle", im)
        #cv2.imshow('image',im)
        #cv2.waitKey(0)
        cv2.imwrite(save_path+s_str+".jpg",im)

cv2.destroyAllWindows()



