import random
import string
import math
import itertools
import os
import time

import numpy as np
import imgaug
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn.model_selection

import keras_ocr
import sys

import sys
import xml.etree.ElementTree as ET

from keras_ocr.tools import augment

assert tf.config.list_physical_devices('GPU'), 'No GPU is available.'
import glob

train_labels_argor=[]

for f in glob.iglob('/home/tera/Desktop/k2_50g/train/sn/*/*/*', recursive=True):
    print(f)
    if not os.path.isfile(f):
        continue
    if not f.endswith('xml'):
        continue
    tree = ET.parse(f)
    root = tree.getroot()
    img_fullpath=root.findall('path')[0].text
    if not os.path.isfile(img_fullpath):
        print("Img not found: " + img_fullpath)
        continue
    for box in root.findall('object'):
        print("----")
        label=box.findall('name')[0].text.lower()
        xmin = box.findall('bndbox')[0].findall('xmin')[0].text
        ymin = box.findall('bndbox')[0].findall('ymin')[0].text
        xmax = box.findall('bndbox')[0].findall('xmax')[0].text
        ymax = box.findall('bndbox')[0].findall('ymax')[0].text
        train_labels_argor.append((img_fullpath,
                                   np.float32(np.array([
                                        [ np.float32(xmin) , np.float32(ymin)],
                                        [ np.float32(xmax) , np.float32(ymin)],
                                        [ np.float32(xmax) , np.float32(ymax)],
                                        [ np.float32(xmin) , np.float32(ymax) ]
                                    ],dtype=np.float32)),
                                   label))
print(train_labels_argor)
train_labels=train_labels_argor
print(len(train_labels))

#alphabet = string.ascii_lowercase + string.digits + '+*=-'
alphabet = string.digits + string.ascii_letters
alphabet = ''.join(sorted(set(alphabet.lower())))

recognizer = keras_ocr.recognition.Recognizer(alphabet=alphabet,weights='kurapan')
recognizer.compile()

for layer in recognizer.backbone.layers:
    layer.trainable = True

batch_size = 32

augmenter = imgaug.augmenters.Sequential([
    imgaug.augmenters.GammaContrast(gamma=(0.9, 1.1)),
#    imgaug.augmenters.Canny(alpha=(0.9, 1.0))
])

train_labels, validation_labels = sklearn.model_selection.train_test_split(train_labels, test_size=0.2, random_state=42)
(training_image_gen, training_steps), (validation_image_gen, validation_steps) = [
    (
        keras_ocr.datasets.get_recognizer_image_generator(
            labels=labels,
            height=recognizer.model.input_shape[1],
            width=recognizer.model.input_shape[2],
            alphabet=recognizer.alphabet,
#            augmenter=augmenter
        ),
        len(labels) // batch_size
    ) for labels, augmenter in [(train_labels, augmenter), (validation_labels, None)]
]
training_gen, validation_gen = [
    recognizer.get_batch_generator(
        image_generator=image_generator,
        batch_size=batch_size
    )
    for image_generator in [training_image_gen, validation_image_gen]
]


while True:
    image, text = next(training_image_gen)
    print('text:', text)
    plt.imshow(image)
    plt.draw()
    if (plt.waitforbuttonpress(-1)):
        break
#sys.exit()

callbacks = [
    tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=100, restore_best_weights=False),
    tf.keras.callbacks.ModelCheckpoint('recognizer_borndigital.h5', monitor='val_loss', save_best_only=True),
    tf.keras.callbacks.CSVLogger('recognizer_borndigital.csv')
]
recognizer.training_model.fit_generator(
    generator=training_gen,
    steps_per_epoch=training_steps,
    validation_steps=validation_steps,
    validation_data=validation_gen,
    callbacks=callbacks,
    epochs=1000,
)







