import os
import numpy as np
import matplotlib.pyplot as plt

from pathlib import Path
from collections import Counter

import tensorflow as tf
from tensorflow import keras
# Path to the data directory
#data_dir = Path("/home/tera/Desktop/CAPTCH/renamed")
import captcha_lib

epochs = 10000
lr=0.01
patience=20
reload_weights=False

data_dir1 = Path("/home/tera/Desktop/chars/generated1")
data_dir2 = Path("/home/tera/Desktop/chars/generated2")
data_dir3 = Path("/home/tera/Desktop/chars/real_images_train_val/")

import string

# Get list of all the images
images1 = sorted(list(map(str, list(data_dir1.glob("*.jpg")))))
labels1 = [img.split(os.path.sep)[-1].split(".jpg")[0] for img in images1]
images2 = sorted(list(map(str, list(data_dir2.glob("*.jpg")))))
labels2 = [img.split(os.path.sep)[-1].split(".jpg")[0] for img in images2]
images3 = sorted(list(map(str, list(data_dir3.glob("*.jpg")))))
labels3 = [img.split(os.path.sep)[-1].split(".jpg")[0] for img in images3]


#characters = set(char for label in labels for char in label)
#characters = set(string.digits+string.ascii_uppercase)

print("MAX LENGHT:")
print (captcha_lib.max_length)
# Mapping characters to integers



# Splitting data into training and validation sets
x1_train, x1_val, y1_train, y1_val = captcha_lib.split_data(np.array(images1), np.array(labels1),train_size=0.1)
x2_train, x2_val, y2_train, y2_val = captcha_lib.split_data(np.array(images2), np.array(labels2),train_size=0.1)
x3_train, x3_val, y3_train, y3_val = captcha_lib.split_data(np.array(images3), np.array(labels3),train_size=0.9)

x_train=[*x1_train,*x2_train,*x3_train]
y_train=[*y1_train,*y2_train,*y3_train]

x_valid=[*x1_val,*x2_val,*x3_val]
y_valid=[*y1_val,*y2_val,*y3_val]



train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_dataset = (
    train_dataset.map(
        captcha_lib.encode_single_sample, num_parallel_calls=tf.data.AUTOTUNE
    )
    .batch(captcha_lib.batch_size)
    .prefetch(buffer_size=tf.data.AUTOTUNE)
)

validation_dataset = tf.data.Dataset.from_tensor_slices((x_valid, y_valid))
validation_dataset = (
    validation_dataset.map(
        captcha_lib.encode_single_sample, num_parallel_calls=tf.data.AUTOTUNE
    )
    .batch(captcha_lib.batch_size)
    .prefetch(buffer_size=tf.data.AUTOTUNE)
)
_, ax = plt.subplots(4, 4, figsize=(10, 5))
for batch in train_dataset.take(1):
    images = batch["image"]
    labels = batch["label"]
    for i in range(16):
        img = (images[i] * 255).numpy().astype("uint8")
        label = tf.strings.reduce_join(captcha_lib.num_to_char(labels[i])).numpy().decode("utf-8")
        ax[i // 4, i % 4].imshow(img[:, :, 0].T, cmap="gray")
        ax[i // 4, i % 4].set_title(label)
        ax[i // 4, i % 4].axis("off")
plt.show()

model=None
# Get the model
if reload_weights:
    model = tf.keras.models.load_model("model_220713b")
else:
    model = captcha_lib.build_model(adam_learning_rate=lr)
    model.summary()


callbacks = [
    tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=patience, restore_best_weights=True),
    tf.keras.callbacks.ModelCheckpoint('captcha.h5', monitor='val_loss', save_best_only=True)
    # tf.keras.callbacks.CSVLogger('recognizer_borndigital.csv')
]
# Train the model

history = model.fit(
    train_dataset,
    validation_data=validation_dataset,
    epochs=epochs,
    callbacks=callbacks
)

model.save("model_220713b")
