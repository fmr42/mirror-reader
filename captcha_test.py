import os
import numpy as np
import matplotlib.pyplot as plt

from pathlib import Path
from collections import Counter

import tensorflow as tf
from tensorflow import keras

# Path to the data directory
#data_dir = Path("/home/tera/Desktop/CAPTCH/renamed")
import captcha_lib

test_data_dir = Path("/home/tera/Desktop/chars/real_images_test/")
#test_data_dir = Path("/home/tera/Desktop/chars/generated2/")




test_images = sorted(list(map(str, list(test_data_dir.glob("*.jpg")))))
test_labels = [img.split(os.path.sep)[-1].split(".jpg")[0] for img in test_images]


test_dataset = tf.data.Dataset.from_tensor_slices((test_images, test_labels))
test_dataset = (
    test_dataset.map(
        captcha_lib.encode_single_sample, num_parallel_calls=tf.data.AUTOTUNE
    )
    .batch(captcha_lib.batch_size)
    .prefetch(buffer_size=tf.data.AUTOTUNE)
)

model=captcha_lib.build_model()
model.load_weights("captcha.h5")
# Get the prediction model by extracting layers till the output layer
prediction_model = keras.models.Model(
    model.get_layer(name="image").input, model.get_layer(name="dense2").output
)
prediction_model.summary()

tot=0
ok=0
#  Let's check results on some validation samples
for batch in test_dataset.take(1):
    batch_images = batch["image"]
    batch_labels = batch["label"]
    preds = prediction_model.predict(batch_images)
    pred_texts = captcha_lib.decode_batch_predictions(preds)
    for i in range(len(pred_texts)):
        tot=tot+1
        label = tf.strings.reduce_join(captcha_lib.num_to_char(batch_labels[i])).numpy().decode("utf-8")
        pred=pred_texts[i]
        print(label," ",pred)
        if(label==pred):
            ok=ok+1
print("OK Images: ", ok , "/", tot)
print("Accuracy is : ", np.round(ok/tot*10000)/100 , "%")

