import cv2
import numpy as np
import os
os.environ['OPENCV_VIDEOIO_PRIORITY_MSMF'] = '0'
from matplotlib import pyplot as plt

cam = cv2.VideoCapture(0)
cv2.namedWindow("test")

img_counter=0
img=[]
while True:
    ret, frame = cam.read()
    if not ret:
        print("failed to grab frame")
        break
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # define range of black color in HSV
    lower_val = np.array([0, 0, 0])
    upper_val = np.array([179, 255, 30])
    mask = cv2.inRange(hsv, lower_val, upper_val)
    #mask_inv = cv2.bitwise_not(mask)
    #blur = cv2.blur(gray, (11 , 11), 0)
    #laplacian = cv2.Laplacian(blur, cv2.CV_64F)
    #laplacian = np.abs(laplacian)
    #laplacian=blur
    #laplacian=np.all(blur < 100, axis=2)
    cv2.imshow("test", mask)

    k = cv2.waitKey(1)
    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img.append(mask)
        img_counter += 1

cam.release()

w=int(320)
h=int(240)
center = (240 , 320)
x = center[1]
y = center[0]
print("center "+str(center))
cv2.imshow("img1", img[0] )

img_crop = img[0][int(y-h/2):int(y+h/2), int(x-w/2):int(x+w/2)]
print([int(y-h/2),int(y+h/2),int(x-w/2),int(x+w/2)])
cv2.imshow("crop", img_crop)

#for i in range(img_counter):
#    print(i)
#    cv2.imshow(str(i), img[i])



# All the 6 methods for comparison in a list
methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']





w, h = img[0].shape[::-1]

# Apply template Matching
res = cv2.matchTemplate(np.uint8(img[0]),np.uint8(img_crop),cv2.TM_CCORR)
cv2.imshow("res",res )
min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
print(max_loc)
# If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum

#if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
#    top_left = min_loc
#else:
top_left = max_loc

bottom_right = (top_left[0] + w, top_left[1] + h)
cv2.rectangle(img[0],top_left, bottom_right, 255, 2)

#img_crop = img[0][int(y-h/2):int(y+h/2), int(x-w/2):int(x+w/2)]
cv2.rectangle(img[0],(int(y-h/2),int(x-w/2)), (int(y+h/2),int(x+w/2)),(255, 255, 0), 1)

cv2.imshow("res",img[0])

cv2.waitKey(0)
cv2.destroyAllWindows()
