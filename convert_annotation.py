

import sys
import xml.etree.ElementTree as ET



for f in sys.argv[1:]:
    nf = open(f+".txt", "w")
    print(f)
    tree = ET.parse(f)
    root = tree.getroot()
    for box in root.findall('object'):
        print("----")
        label=box.findall('name')[0].text
        xmin = box.findall('bndbox')[0].findall('xmin')[0].text
        ymin = box.findall('bndbox')[0].findall('ymin')[0].text
        xmax = box.findall('bndbox')[0].findall('xmax')[0].text
        ymax = box.findall('bndbox')[0].findall('ymax')[0].text
        print(label)
        print(xmin)
        print(ymin)
        print(xmax)
        print(ymax)
        nf.write(xmin+","+ymin+","+xmax+","+ymin+","+xmax+","+ymax+","+xmin+","+ymax+","+label+"\n")
    nf.close()

